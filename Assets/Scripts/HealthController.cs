﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthController : MonoBehaviour
{

    private PlayerController player;
    private Text text;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        string health = "";
        for (int i = 0; i < player.health; i++)
        {
            health += "️❤️";
        }
        text.text = health;
    }
}
