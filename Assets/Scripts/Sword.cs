using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour, IItem
{
    public GameObject bubble;
    private PlayerController player;
    private enum SwordState { Ready, Active, Cooldown };
    private SwordState currentSwordState;
    private Vector3 target;

    private Inventory inventory;

    public Sprite[] images;
    public float attackDuration;
    private float attackCounter;
    private Animator animator;

    private int[] costs = new int[] { 50, 100, 250, 750 };

    private void Start()
    {
        itemLevel = 0;
        damage = 1;
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        inventory = player.GetComponent<Inventory>();
        currentImage = images[itemLevel];
        currentSwordState = SwordState.Ready;
        animator = GetComponent<Animator>();

    }

    private void FixedUpdate()
    {
        if (transform.parent == null)
            return;
        if (attackCounter > attackDuration)
        {
            currentSwordState = SwordState.Ready;
            attackCounter = 0;
        }
        if (currentSwordState != SwordState.Ready)
            attackCounter += Time.deltaTime;

        if (currentSwordState == SwordState.Active)
        {
            animator.SetTrigger("Hit");

            currentSwordState = SwordState.Cooldown;
        }
    }

    public int itemLevel { get; set; }
    public int damage { get; set; }

    public Sprite currentImage { get; set; }

    public void Upgrade()
    {
        bool success = inventory.Buy(costs[itemLevel]);
        if (!success)
            return;
        itemLevel += 1;
        if (itemLevel == 4)
            GameObject.Find("SwordButton").SetActive(false);
        damage *= 2;
        currentImage = images[itemLevel];
        GetComponent<SpriteRenderer>().sprite = currentImage;

    }



    public void DoAction(PlayerController.Direction direction)
    {
        if (currentSwordState == SwordState.Ready)
        {
            switch (player.currentDirection)
            {
                case PlayerController.Direction.Up:
                    target = new Vector3(0, 0, 90);
                    break;
                case PlayerController.Direction.Down:
                    target = new Vector3(0, 0, -90);
                    break;
                case PlayerController.Direction.Left:
                    target = new Vector3(0, 0, 180);
                    break;
                case PlayerController.Direction.Right:
                    target = new Vector3(0, 0, 0);
                    break;
                default: break;
            }
            transform.rotation = Quaternion.Euler(target);
            currentSwordState = SwordState.Active;
        }
    }



}

