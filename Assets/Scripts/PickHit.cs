﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickHit : MonoBehaviour
{
    private Pick pick;
    // Start is called before the first frame update
    void Start()
    {
        pick = GetComponentInParent<Pick>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        pick.ForwardedTriggerEnter2d(other);
    }
}
