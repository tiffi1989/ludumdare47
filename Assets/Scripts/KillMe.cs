﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMe : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Kill", .3f);
    }

    public void Kill()
    {
        GameObject.Destroy(gameObject);
    }
}
