using UnityEngine;
using System.Collections;

public interface IItem
{
    int itemLevel { get; set; }
    int damage { get; set; }

    Sprite currentImage { get; set; }

    void DoAction(PlayerController.Direction direction);

}