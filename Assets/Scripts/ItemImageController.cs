﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemImageController : MonoBehaviour
{
    public Image itemImage;
    private GameObject player;
    private int count;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        count++;
        if (count > 10)
        {
            itemImage.sprite = player.GetComponentInChildren<IItem>().currentImage;
        }
    }

    public void UpdateItemImage(Sprite image)
    {
        itemImage.sprite = image;
    }
}
