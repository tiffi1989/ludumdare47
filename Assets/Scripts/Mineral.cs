﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mineral : MonoBehaviour
{
    public string type;
    public Sprite[] sprites;
    public int structurePoints { get; set; }
    private ParticleSystem particle;
    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
        particle = GetComponentInChildren<ParticleSystem>();
        switch (type)
        {
            case "blocker":
                structurePoints = 2;
                break;
            case "copper":
                structurePoints = 5;
                break;
            case "iron":
                structurePoints = 15;
                break;
            case "gold":
                structurePoints = 50;
                break;
            case "diamond":
                structurePoints = 200;
                break;
            case "exit":
                structurePoints = 2000;
                break;
            default: break;
        }
    }

    public void Hit(int damage)
    {
        structurePoints -= damage;
        particle.Play();
    }

}
