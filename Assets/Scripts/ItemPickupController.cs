﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ItemPickupController : MonoBehaviour
{
    private PlayerController playerController;
    private GameObject player;
    public enum PickupState { Ready, Active, Cooldown }
    public PickupState currentPickupState = PickupState.Ready;
    private float pickupCounter;
    private float pickupCooldown = .5f;

    public Text text;

    void Start()
    {
        playerController = GetComponentInParent<PlayerController>();
        player = GameObject.Find("Player");
    }

    void Update()
    {
        if (currentPickupState == PickupState.Cooldown)
        {
            pickupCounter += Time.deltaTime;
        }

        if (pickupCounter > pickupCooldown)
        {
            pickupCounter = 0;
            currentPickupState = PickupState.Ready;
        }
        if (Input.GetButton("Jump") && currentPickupState != PickupState.Cooldown)
        {
            currentPickupState = PickupState.Active;
        }
        else if (currentPickupState != PickupState.Cooldown)
            currentPickupState = PickupState.Ready;

    }

    private void SwitchItems(GameObject oldItem, GameObject newItem)
    {
        Debug.Log(newItem.transform.position);
        oldItem.GetComponent<SpriteRenderer>().enabled = true;
        oldItem.GetComponent<SpriteRenderer>().sprite = oldItem.GetComponent<IItem>().currentImage;
        oldItem.GetComponent<CircleCollider2D>().enabled = false;
        oldItem.GetComponent<PolygonCollider2D>().enabled = true;
        oldItem.transform.parent = null;
        oldItem.transform.position = new Vector3(newItem.transform.position.x, newItem.transform.position.y, newItem.transform.position.z);

        newItem.GetComponent<SpriteRenderer>().enabled = false;
        newItem.GetComponent<CircleCollider2D>().enabled = true;
        newItem.GetComponent<PolygonCollider2D>().enabled = false;
        newItem.transform.parent = player.transform;
        newItem.transform.localPosition = Vector3.zero;
        playerController.currentItem = newItem.GetComponent<IItem>();
    }

    private void OnTriggerStay2D(Collider2D other)
    {

        if (other.gameObject.tag == "Sword")
            text.text = "Press space to pick up the sword";
        if (other.gameObject.tag == "Pick")
            text.text = "Press space to pick up the Pickaxe";

        if (other.gameObject.tag == "Sword" && playerController.currentItem.GetType() == typeof(Pick) && currentPickupState == PickupState.Active)
        {
            SwitchItems(GameObject.Find("Pick"), other.gameObject);
            currentPickupState = PickupState.Cooldown;
        }

        if (other.gameObject.tag == "Pick" && playerController.currentItem.GetType() == typeof(Sword) && currentPickupState == PickupState.Active)
        {
            SwitchItems(GameObject.Find("Sword"), other.gameObject);
            currentPickupState = PickupState.Cooldown;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Pick" || other.gameObject.tag == "Sword")
            text.text = "";
    }
}
