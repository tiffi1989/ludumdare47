﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToastController : MonoBehaviour
{

    private Text text;
    private bool fading;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (text.color.a > .02f && fading)
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - .02f);
    }

    public void addToast(string message)
    {
        if (text.color.a > .1f)
            text.text = text.text + "\n" + message;
        else
            text.text = message;
        text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
        Invoke("StartFading", 1f);
        fading = false;
    }

    public void StartFading()
    {
        fading = true;
    }
}
