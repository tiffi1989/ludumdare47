﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public enum Direction { Up, Down, Left, Right }
    public float speed, maxSpeed;
    private float horizontal, vertical;
    public bool dead;
    private Rigidbody2D rig;
    private CircleCollider2D col;
    public Direction currentDirection = Direction.Down;

    public SpriteRenderer mainSprite;

    public Sprite left, right, back;
    public IItem currentItem;
    public int health;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        col = GetComponent<CircleCollider2D>();
        currentItem = GetComponentInChildren<IItem>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
    }


    void FixedUpdate()
    {
        if (dead)
            return;
        if (rig.velocity.magnitude < maxSpeed && !dead)
            rig.AddForce(new Vector2(horizontal, vertical) * speed);
        if (Input.GetKeyDown(KeyCode.Return) && dead)
            SceneManager.LoadScene("MainScene");
        if (dead)
            mainSprite.enabled = false;
        SetDirection();
        Action();
        if (health <= 0)
            dead = true;
    }

    void Action()
    {
        if (Input.GetButton("Fire1") && currentItem != null)
        {
            currentItem.DoAction(currentDirection);
        }
    }



    private void SetDirection()
    {
        if (vertical > 0)
        {
            currentDirection = Direction.Up;
            mainSprite.sprite = back;
        }
        if (vertical < 0)
        {
            currentDirection = Direction.Down;
            mainSprite.sprite = right;
        }
        if (horizontal > 0)
        {
            currentDirection = Direction.Right;
            mainSprite.sprite = right;
        }
        if (horizontal < 0)
        {
            currentDirection = Direction.Left;
            mainSprite.sprite = left;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Darkness")
        {
            dead = true;
            mainSprite.enabled = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag == "Enemy")
        {
            health -= 1;
            audioSource.Play();
        }
    }
}
