﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    private Transform player;

    private ParticleSystem particle;
    public float speed;

    public float maxSpeed;

    public int health;
    private Rigidbody2D rig;

    private Sword sword;

    public float sightRange;
    private AudioSource audioSource;

    private Inventory inventory;

    public GameObject killParticle;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
        inventory = GameObject.Find("Player").GetComponent<Inventory>();
        rig = GetComponent<Rigidbody2D>();
        sword = GameObject.Find("Sword").GetComponent<Sword>();
        particle = GetComponentInChildren<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rig.velocity.magnitude < maxSpeed && Vector3.Distance(player.position, transform.position) < sightRange)
        {
            rig.AddForce((player.position - transform.position).normalized * speed);
        }

        if (rig.velocity.x > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }



    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Sword")
        {
            health -= sword.damage;
            audioSource.Play();
            particle.gameObject.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(other.gameObject.transform.position.y - transform.position.y, other.gameObject.transform.position.x - transform.position.x) * 180 / Mathf.PI + 180);
            rig.AddForce((transform.position - player.transform.position).normalized * 10 * sword.damage);
            if (health <= 0)
            {
                Instantiate(killParticle, transform.position, Quaternion.identity);
                inventory.add("money");
                GameObject.Destroy(gameObject.transform.parent.gameObject);
            }
            else
                particle.Play();
        }
    }

}
