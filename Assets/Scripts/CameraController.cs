﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float mapTop, mapBottom, mapLeft, mapRight;

    private GameObject player;
    private Vector3 playerPostion;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        playerPostion = player.transform.position;
        Camera cam = Camera.main;
        float height = 2f * cam.orthographicSize;
        float width = height * cam.aspect;
        if (playerPostion.y + height / 2 < mapTop && playerPostion.y - height / 2 > mapBottom)
            transform.position = new Vector3(transform.position.x, playerPostion.y, transform.position.z);
        if (playerPostion.x + width / 2 < mapRight && playerPostion.x - width / 2 > mapLeft)
            transform.position = new Vector3(playerPostion.x, transform.position.y, transform.position.z);

    }
}
