﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public GameObject blocker, copper, iron, gold, diamond;

    private GameObject mineralInstance;

    public int rarity;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GameObject newMineral = GetRandomMineral();
        if (newMineral != null)
            mineralInstance = Instantiate(newMineral, gameObject.transform.position, Quaternion.identity);
    }

    private GameObject GetRandomMineral()
    {
        float rnd = Random.Range(-5f, 5f);
        rnd += rarity;
        if (rnd > 9.5f)
            return diamond;
        if (rnd > 9f)
            return gold;
        if (rnd > 6)
            return iron;
        if (rnd > 2)
            return copper;
        if (rnd > -1)
            return blocker;

        return null;

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Shadow")
        {
            if (mineralInstance != null)
                GameObject.Destroy(mineralInstance);
            GameObject newMineral = GetRandomMineral();
            if (newMineral != null)
                mineralInstance = Instantiate(newMineral, gameObject.transform.position, Quaternion.identity);
        }
    }
}
