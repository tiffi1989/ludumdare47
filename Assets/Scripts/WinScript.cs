﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine;

public class WinScript : MonoBehaviour
{

    public GameObject win;
    public GameObject globalLight;
    public GameObject[] toBeDeactivated;

    private bool won;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (won)
        {
            for (int i = 0; i < toBeDeactivated.Length; i++)
            {
                toBeDeactivated[i].SetActive(false);
            }
            globalLight.SetActive(true);
            globalLight.GetComponent<Light2D>().intensity += .03f;
        }
    }

    public void ShowEnd()
    {
        win.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            won = true;

            Invoke("ShowEnd", 2f);
        }
    }
}
