﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public GameObject enemy;
    private GameObject enemyInstance;

    private float timer;
    private bool startSpawning;

    private float randomPauseTime;

    public float startSpawningTime;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        startSpawningTime += Random.Range(0f, 30f);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > startSpawningTime && timer > randomPauseTime)
        {
            startSpawning = true;
            timer = 0;
        }
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Shadow")
        {
            if (enemyInstance == null && startSpawning)
            {
                enemyInstance = Instantiate(enemy, transform.position, Quaternion.identity);
                int randomHealth = Random.Range(3, 16);

                enemyInstance.GetComponentInChildren<Enemy>().health = randomHealth;
                enemyInstance.transform.localScale = new Vector3(1 * (randomHealth / 7f), 1 * (randomHealth / 7f), 1);
                startSpawning = false;
                randomPauseTime = Random.Range(0f, 60f);
            }
        }
    }
}
