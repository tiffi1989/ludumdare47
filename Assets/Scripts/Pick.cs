using UnityEngine;
using System.Collections;

public class Pick : MonoBehaviour, IItem
{
    public GameObject bubble;
    private PlayerController player;
    private enum PickState { Ready, Active, Cooldown };
    private PickState currentPickState;
    private Vector3 target;

    private Inventory inventory;

    public Sprite[] images;
    public float pickDuration;
    private float pickCounter;
    private bool alreadyHitSomething;
    private AudioSource audioSource;
    private Animator animator;

    private int[] costs = new int[] { 50, 100, 300, 1000 };
    private void Start()
    {
        itemLevel = 0;
        damage = 1;
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        inventory = player.GetComponent<Inventory>();
        currentImage = images[itemLevel];
        currentPickState = PickState.Ready;
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (transform.parent == null)
            return;
        if (pickCounter > pickDuration)
        {
            currentPickState = PickState.Ready;
            pickCounter = 0;
            alreadyHitSomething = false;
        }
        if (currentPickState != PickState.Ready)
            pickCounter += Time.deltaTime;

        if (currentPickState == PickState.Active)
        {
            animator.SetTrigger("Hit");

            currentPickState = PickState.Cooldown;
        }
    }

    public int itemLevel { get; set; }
    public int damage { get; set; }

    public Sprite currentImage { get; set; }

    public void Upgrade()
    {

        bool success = inventory.Buy(costs[itemLevel]);
        if (!success)
            return;
        itemLevel += 1;
        if (itemLevel == 4)
            GameObject.Find("PickButton").SetActive(false);
        damage = (damage + itemLevel) * 2;
        currentImage = images[itemLevel];
        GetComponent<SpriteRenderer>().sprite = currentImage;
    }


    public void DoAction(PlayerController.Direction direction)
    {
        if (currentPickState == PickState.Ready)
        {
            switch (player.currentDirection)
            {
                case PlayerController.Direction.Up:
                    target = new Vector3(0, 0, 90);
                    break;
                case PlayerController.Direction.Down:
                    target = new Vector3(0, 0, -90);
                    break;
                case PlayerController.Direction.Left:
                    target = new Vector3(0, 0, 180);
                    break;
                case PlayerController.Direction.Right:
                    target = new Vector3(0, 0, 0);
                    break;
                default: break;
            }
            transform.rotation = Quaternion.Euler(target);

            currentPickState = PickState.Active;
        }
    }

    public void ForwardedTriggerEnter2d(Collider2D other)
    {
        if (other.tag == "Mineral" && !alreadyHitSomething)
        {
            audioSource.Play();

            alreadyHitSomething = true;
            int newSp = other.gameObject.GetComponent<Mineral>().structurePoints - damage;
            Debug.Log("SP: " + other.gameObject.GetComponent<Mineral>().structurePoints + " newSP: " + newSp + " damage: " + damage + " bubbleText: " + (((int)(newSp / damage)) + 1));
            if (newSp > 0)
            {
                GameObject newBubble = Instantiate(bubble, other.gameObject.transform.position, Quaternion.identity);
                other.gameObject.GetComponent<Mineral>().Hit(damage);
                newBubble.GetComponentInChildren<TextMesh>().text = (newSp % damage > 0 ? ((int)(newSp / damage) + 1) : ((int)(newSp / damage))).ToString();
            }
            else
            {
                inventory.add(other.gameObject.GetComponent<Mineral>().type);
                GameObject.Destroy(other.gameObject);
            }
        }
    }

}

