﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryText : MonoBehaviour
{

    private Text text;
    private Inventory inventory;
    // Start is called before the first frame update
    void Start()
    {
        inventory = GameObject.Find("Player").GetComponent<Inventory>();
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        text.text = inventory.stones + "x Stones\n" + inventory.copper + "x Copper\n" + inventory.iron + "x iron\n" + inventory.gold + "x gold\n" + inventory.diamonds + "x diamonds\n" + inventory.money + " $";

    }
}
