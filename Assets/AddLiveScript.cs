﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddLiveScript : MonoBehaviour
{

    private PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (player.health < 3)
            player.health += 1;
    }
}
