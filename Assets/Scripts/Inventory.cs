﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Inventory : MonoBehaviour
{
    public int stones;
    public int copper;
    public int iron;
    public int gold;
    public int diamonds;

    public int money;

    public Animator animator;

    public ToastController toasts;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (animator.GetBool("Open"))
            {
                animator.SetBool("Open", false);
                Time.timeScale = 1f;
            }
            else
            {
                animator.SetBool("Open", true);
                Invoke("Pause", .4f);
            }


        }
    }

    public void SellAll()
    {
        money += 1 * stones;
        money += 5 * copper;
        money += 20 * iron;
        money += 75 * gold;
        money += 200 * diamonds;
        stones = copper = iron = gold = diamonds = 0;
    }

    public void Pause()
    {
        Time.timeScale = 0.1f;

    }

    public bool Buy(int cost)
    {
        if (money < cost)
        {
            toasts.addToast((cost - money).ToString() + "$ more needed");
            return false;
        }
        else
        {
            money -= cost;
            return true;
        }
    }


    public void add(string type)
    {

        switch (type)
        {
            case "blocker":
                stones += 1;
                toasts.addToast("+1 Stone");
                break;
            case "copper":
                copper += 1;
                toasts.addToast("+1 copper");
                break;
            case "iron":
                iron += 1;
                toasts.addToast("+1 iron");
                break;
            case "gold":
                gold += 1;
                toasts.addToast("+1 gold");
                break;
            case "diamond":
                diamonds += 1;
                toasts.addToast("+1 diamond");
                break;
            case "money":
                money += 25;
                toasts.addToast("+25 $");
                break;
            default: break;
        }

    }
    public void remove(string type, int amount)
    {

        switch (type)
        {
            case "blocker":
                stones -= amount;
                toasts.addToast("-" + amount + " Stone");
                break;
            case "copper":
                copper -= amount;
                toasts.addToast("-" + amount + " copper");
                break;
            case "iron":
                iron -= amount;
                toasts.addToast("-" + amount + " iron");
                break;
            case "gold":
                gold -= amount;
                toasts.addToast("-" + amount + " gold");
                break;
            case "diamond":
                diamonds -= amount;
                toasts.addToast("-" + amount + " diamond");
                break;
            default: break;
        }
    }
}
